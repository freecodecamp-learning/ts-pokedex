const container: HTMLElement | any = document.getElementById("app");
const pokemons: number = 100;
let pokemonCollection: Array<IPokemon> = new Array(pokemons);

interface IPokemon {
  id: number;
  name: string;
  image: string;
  type: string;
}

const run = async (): Promise<void> => {
  await fetchData();
  showPokemon(pokemonCollection);
};

const fetchData = async (): Promise<void> => {
  for (let i = 1; i <= pokemons; i++) {
    try {
      const response = await getPokemon(i);
      pokemonCollection.push(response);
    } catch (error) {
      console.error(error);
    }
  }
};

const getPokemon = async (id: number): Promise<IPokemon> => {
  const data: Response = await fetch(`https://pokeapi.co/api/v2/pokemon/${id}`);
  const pokemon: any = await data.json();
  const pokemonType: string = pokemon.types
    .map((poke: any) => poke.type.name)
    .join(", ");

  const transformedPokemon = {
    id: pokemon.id,
    name: pokemon.name,
    image: `${pokemon.sprites.front_default}`,
    type: pokemonType,
  };

  return transformedPokemon;
};

const showPokemon = (pokemonCollection: IPokemon[]): void => {
  pokemonCollection.forEach((pokemon) => {
    let output: string = `
        <div class="card">
            <span class="card--id">#${pokemon.id}</span>
            <img class="card--image" src="${pokemon.image}" alt="${pokemon.name}" />
            <h1 class="card--name">${pokemon.name}</h1>
            <span class="card--details">${pokemon.type}</span>
        </div>
    `;

    container.innerHTML += output;
  });
};

run();
